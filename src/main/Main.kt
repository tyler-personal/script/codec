package main

import domain.Driver
import domain.boundaries.IO
import presentation.ConsoleIO

fun main(args: Array<String>) {
    val io: IO = ConsoleIO()
    Driver(io).perform()
}