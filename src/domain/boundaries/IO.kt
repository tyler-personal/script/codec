package domain.boundaries

interface IO {
    fun show(out: String)
    fun input(): String

    fun input(out: String): String {
        show(out)
        return input()
    }
}