package domain

import domain.boundaries.IO
import domain.extensions.runCommand
import java.io.File

class Driver(private val io: IO) {
    private val fileTypes = "mpeg mkv avi mp4 flv".split(" ")

    fun perform() {
        val dir = getDir(io.input("What's your path? (start at root)"))

        dir.walk().filter { fileTypes.contains(it.extension) }.forEach { file ->
            "ffmpeg -i \"${file.name}\" \"${file.nameWithoutExtension}.mp4\"".runCommand(dir)
        }
    }

    private fun getDir(path: String): File =
        File(path).takeIf(File::exists) ?: getDir("Dir doesn't exist, try again.")
}