package presentation

import domain.boundaries.IO

class ConsoleIO: IO {
    override fun input() = readLine()!!
    override fun show(out: String) = println(out)
}